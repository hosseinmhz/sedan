*BEGIN
query;1
subject;?x
predicate;http://semmed.org/property/CAUSES
object;http://purl.obolibrary.org/obo/Katayama_fever
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

*BEGIN
query;2
subject;?x
predicate;http://semmed.org/property/CAUSES
object;http://purl.obolibrary.org/obo/episodic_ataxia_type_6
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;5
END;

*BEGIN
query;3
subject;http://bio2rdf.org/drugbank:Statins
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Diabetes
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

*BEGIN
query;4
subject;http://semmed.org/resource/Levothyroxine_Sodium
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Primary_Insomnia
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

*BEGIN
query;5
subject;?x
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Riedel's_thyroiditis
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

*BEGIN
query;6
subject;http://semmed.org/resource/Herceptin
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Prostate_cancer_metastatic
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

*BEGIN
query;6
subject;http://semmed.org/resource/Herceptin
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Prostate_cancer_metastatic
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

*BEGIN
query;7
subject;?x
predicate;http://semmed.org/property/TREATS
object;http://purl.obolibrary.org/obo/acute_myocarditis
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

*BEGIN
query;8
subject;?x
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Delayed_Sleep_Phase_Syndrome
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

*BEGIN
query;9
subject;http://semmed.org/resource/DDX54
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/DNA_Damage
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

*BEGIN
query;9
subject;http://semmed.org/resource/DDX54
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/DNA_Damage
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

*BEGIN
query;10
subject;http://semmed.org/resource/Secondary_post_tonsillectomy_hemorrhage
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Voice_Disorders
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

*BEGIN
query;11
subject;http://semmed.org/resource/RNAIII
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Amyloidosis
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

*BEGIN
query;12
subject;http://semmed.org/resource/RNAIII
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Injury_to_kidney_NOS
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

*BEGIN
query;13
subject;http://bio2rdf.org/drugbank:Dasatinib
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Glioblastoma
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

*BEGIN
query;14
subject;http://semmed.org/resource/Immunoglobulins,_Intravenous
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Alzheimer's_Disease
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

*BEGIN
query;15
subject;http://bio2rdf.org/drugbank:Davunetide
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Supranuclear_Palsy,_Progressive
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

*BEGIN
query;16
subject;?x
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Erythrasma
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

*BEGIN
query;17
subject;http://semmed.org/resource/Moraxella_Infections
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Respiratory_Tract_Infections
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

*BEGIN
query;17
subject;http://semmed.org/resource/Moraxella_Infections
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Respiratory_Tract_Infections
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;18
subject;http://semmed.org/resource/Antineoplastic_Agents
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Alzheimer's_Disease
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;19
subject;?x
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/stage,_pancreatic_cancer
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;20
subject;http://semmed.org/resource/transglutaminase_2
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Celiac_Disease
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;20
subject;http://semmed.org/resource/transglutaminase_2
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Celiac_Disease
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;21
subject;http://semmed.org/resource/Doxycycline
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Photosensitivity
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;21
subject;http://semmed.org/resource/Doxycycline
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Photosensitivity
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;22
subject;?x
predicate;http://semmed.org/property/CAUSES
object;http://purl.obolibrary.org/obo/black_lung
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;5
END;

# already d=3 of Subject
BEGIN
query;23
subject;http://semmed.org/resource/Glycosides
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Euglycemic_Diabetic_Ketoacidosis
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

# already d=1 of Object
BEGIN
query;231
subject;http://bio2rdf.org/drugbank:Canagliflozin
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Diabetic_Ketoacidosis
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;5
END;

BEGIN
query;24
subject;?x
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Retinal_arterial_tortuosity
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;25
subject;http://semmed.org/resource/CD55
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Thrombosis
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;26
subject;http://semmed.org/resource/CASQ2
predicate;http://semmed.org/property/CAUSES
object;?x
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;27
subject;http://semmed.org/resource/Mitochondrial_DNA_mutation
predicate;http://semmed.org/property/CAUSES
object;?x
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;28
subject;?x
predicate;http://semmed.org/property/CAUSES
object;http://purl.obolibrary.org/obo/woolsorters_disease
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

BEGIN
query;29
subject;http://purl.obolibrary.org/obo/hexosaminidase_A_deficiency
predicate;http://semmed.org/property/CAUSES
object;?x
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

BEGIN
query;30
subject;http://semmed.org/resource/Brucella_abortus_infection
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Spontaneous_abortion
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;301
subject;http://semmed.org/resource/Brucella_abortus_infection
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Spontaneous_abortion
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;31
subject;http://semmed.org/resource/enlimomab
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Stroke,_Acute
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;311
subject;http://semmed.org/resource/enlimomab
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Stroke,_Acute
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;32
subject;http://semmed.org/resource/rituximab
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Chronic_Fatigue_Syndrome
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;321
subject;http://semmed.org/resource/rituximab
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Chronic_Fatigue_Syndrome
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;33
subject;http://semmed.org/resource/Adenovirus_Vector
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Unspecified_hereditary_retinal_dystrophies
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;34
subject;http://bio2rdf.org/drugbank:Dinutuximab
predicate;http://semmed.org/property/TREATS
object;?x
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

*BEGIN
query;35
subject;?x
predicate;http://semmed.org/property/CAUSES
object;http://purl.obolibrary.org/obo/Phthiriasis_Palpebrarum
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

*BEGIN
query;36
subject;http://purl.obolibrary.org/obo/Orteronel
predicate;http://semmed.org/property/TREATS
object;?x
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

BEGIN
query;37
subject;http://bio2rdf.org/drugbank:Matuzumab
predicate;http://semmed.org/property/TREATS
object;?x
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

BEGIN
query;37
subject;http://bio2rdf.org/drugbank:Nivolumab
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Non-small_cell_lung_cancer_stage_II
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

BEGIN
query;38
subject;http://bio2rdf.org/drugbank:Nivolumab
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Non-small_cell_lung_cancer_stage_II
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

*BEGIN
query;39
subject;http://bio2rdf.org/drugbank:Lambrolizumab
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/melanoma
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

BEGIN
query;40
subject;http://bio2rdf.org/drugbank:Afamelanotide
predicate;http://semmed.org/property/TREATS
object;?x
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

BEGIN
query;41
subject;http://bio2rdf.org/drugbank:Vedolizumab
predicate;http://semmed.org/property/TREATS
object;?x
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

BEGIN
query;42
subject;http://bio2rdf.org/drugbank:Migalastat
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Fabry_Disease
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;5
END;

BEGIN
query;422
subject;http://semmed.org/resource/Piperidines
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Fabry_Disease
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;43
subject;http://bio2rdf.org/drugbank:Ocrelizumab
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Multiple_Sclerosis
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

BEGIN
query;44
subject;http://semmed.org/resource/Atypical_neuroleptic
predicate;http://semmed.org/property/TREATS
object;?x
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

BEGIN
query;45
subject;http://semmed.org/resource/Tretinoin
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Photoaging
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;4
END;

BEGIN
query;45
subject;http://semmed.org/resource/Tretinoin
predicate;http://semmed.org/property/TREATS
object;http://semmed.org/resource/Photoaging
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;

*BEGIN
query;46
subject;http://semmed.org/resource/Arimidex
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Hot_flushes
elementtochagne;Subject
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;2
END;

BEGIN
query;46
subject;http://semmed.org/resource/Arimidex
predicate;http://semmed.org/property/CAUSES
object;http://semmed.org/resource/Hot_flushes
elementtochagne;Object
patterns;Gen,Spec,Sim,Dis,Intp,Afort,
depth;3
END;
